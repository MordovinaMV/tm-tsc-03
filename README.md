# TASK MANAGER

JAVA SCREENSHOTS

https://disk.yandex.ru/d/oZNCht7O8zU5Uw?w=1

## DEVELOPER INFO

name: Mordovina Mary

e-mail: mmordovina01@gmail.com

## HARDWARE

CPU: i5

RAM: 16GB

SSD: 512GB

## SOFTWARE

system: Windows 10.0.14393

version JDK: 1.8.0_282


## PROGRAM RUN

```bash
java -jar ./task-manager.jar
```
